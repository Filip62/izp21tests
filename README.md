# izp21tests

Tests for the 2021 IZP team project

If you have any questions or issues with the scrit please contact the author on discord: kam29#4080

## WARNING

Author of this script doesn't guarantee the correctness of the tests or the testing script itself.

## Description

This python script is designed to test the functionality of the 2021 IZP team project by using direct and random tests.

Each test prepares input for the setcal executable, calls it with this input and than checks the correctness of the results.

With default setting, direct tests for every command are run. Specific commands can be selected or excluded by using the script arguments.

Random tests can be enabled by script arguments as well, but be aware that chance of getting positive results on some commands are very small (for example bijective command).

This script doesn't test the bonus implementation.

Tested with python 3.6

## Usage

All arguments of the script are described in the help text:

python3 setcal.py --help
